<?php
namespace Controllers;

use LIB\Controller;
/**
 * Description of About_Controller
 *
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 */
class About extends Controller {

    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $this->getView()->render('About', 'index');
    }
}
