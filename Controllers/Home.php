<?php
namespace Controllers;

use LIB\Controller;
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 28-jun-2015
 * Time: 14:28:05
 */
class Home extends Controller {

    function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $this->getView()->render('Home', 'index');
    }
    public function param($a, $b) {
        echo sprintf('%d argumenten\n', func_num_args());
        echo "$a, $b\n";
        printf("de 3de param is %s", func_get_arg(2));
    }
}