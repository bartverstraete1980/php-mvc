<?php
namespace Controllers;

use LIB\Config;
use LIB\Controller;
use LIB\Session;
/**
 * Description of Login_Controller
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class Login extends Controller {
    public function __construct() {
        parent::__construct();
        $this->cssPath = 'Views/Login/css';
        $this->jsPath = 'Views/Login/js';
    }
    public function index() {
        $this->getView()->render('Login', 'index');
    }
    public function run() {
        echo 'from run()';
        $this->getModel()->login();
    }
    public function logout() {
        Session::destroy();
        header(sprintf('Location: %s', Config::getInstance()->getRoot()));
        exit();
    }
    public function css($file) {
        $c = __CLASS__;
        $a = explode('\\', $c);
        $class = array_pop($a);
        $fp = "Views/$class/css/$file";
        if (file_exists($fp)) {
            header('Content-type: text/css');
            readfile($fp);
        }
    }
    public function js($file) {
        $c = __CLASS__;
        $a = explode('\\', $c);
        $class = array_pop($a);
        $fp = "Views/$class/js/$file";
        if (file_exists($fp)) {
            header('Content-type: text/javascript');
            readfile($fp);
        }
    }
}
