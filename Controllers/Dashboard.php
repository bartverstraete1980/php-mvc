<?php
namespace Controllers;

use LIB\Config;
use LIB\Controller;
use LIB\Session;
/**
 * Description of Dashboard_Controller
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class Dashboard extends Controller {
    public function __construct() {
        parent::__construct();
        $loggedin = Session::get('loggedin');
        if (!$loggedin) {
            Session::destroy();
            header(sprintf('Location: %s', Config::getInstance()->getRoot('Login/')));
            exit();
        }
    }
    public function index() {
        $this->getView()->render('Dashboard', 'index');
    }
}
