<?php
namespace Views\Dashboard;

use phphtml5\Basic\H3;
use Views\DefaultView;
/**
 * Description of Dashboard_index_View
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class index extends DefaultView {
    public function __construct() {
        parent::__construct('Dashboard');
        $this->getHeader()->appendElement(new H3('Dashboard'));
    }
}
