<?php
namespace Views\Home;

use phphtml5\Basic\H3;
use Views\DefaultView;
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 28-jun-2015
 * Time: 17:57:56
 */
class index extends DefaultView {
    function __construct() {
        parent::__construct();
        $this->getHeader()->appendElement(new H3('Home'));
    }
}