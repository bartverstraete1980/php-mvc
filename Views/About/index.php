<?php
namespace Views\About;

use phphtml5\Basic\H3;
use Views\DefaultView;

/**
 * Description of About_index_View
 *
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 */
class index extends DefaultView {
    function __construct() {
        parent::__construct('About');
        $this->getHeader()->appendElement(new H3('About'));
    }
}