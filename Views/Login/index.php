<?php
namespace Views\Login;

use LIB\Config;
use phphtml5\Basic\Form;
use phphtml5\Basic\H3;
use phphtml5\Basic\LabelPassword;
use phphtml5\Basic\LabelText;
use phphtml5\Basic\SubmitButton;
use phphtml5\Enums\MediaType;
use phphtml5\Enums\Method;
use phphtml5\Enums\Rel;
use phphtml5\Tags\LinkTag;
use phphtml5\Tags\ScriptTag;
use Views\DefaultView;
/**
 * Description of Login_index_View
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class index extends DefaultView {
    public function __construct() {
        parent::__construct('Login');
        $this->addCss();
        $this->addJs();
        $this->getHeader()->appendElement(new H3('Login'));
        $this->getContent()->appendElement($form = new Form(('Login'),
                Config::getInstance()->getRoot('Login/run'),
                new Method(Method::POST)));
        $form->appendElement($username = new LabelText($form->getId(), 
                'Username', 'username', $form, TRUE));
        $form->appendElement($password = new LabelPassword($form->getId(),
                'Password', 'password', $form));
        $form->appendElement($submitbutton = new SubmitButton('Login'));
        $submitbutton->setName('Login');
    }
    public function addCss() {
        $list = array_diff(scandir("Views/Login/css/"), array('.', '..'));
        foreach ($list as $file) {
            $this->getHead()->appendElement($link  = new LinkTag(NULL,
            Config::getInstance()->getRoot("Login/css/$file"), NULL, NULL,
            new Rel(Rel::STYLESHEET), NULL, new MediaType(MediaType::TEXTCSS)));
            $link->setId($file);
        }
    }
    public function addJs() {
        $list = array_diff(scandir("Views/Login/js/"), array('.', '..'));
        foreach ($list as $file) {
            $this->getHead()->appendElement($script  = new ScriptTag(NULL, NULL, NULL,
            Config::getInstance()->getRoot("Login/js/$file"),
            new MediaType(MediaType::TEXTJAVASCRIPT)));
            $script->setId($file);
        }
    }
}
