<?php
namespace Views;

use Exception;
use jQueryUISwitcher\jQueryUISwitcher;
use LIB\Config;
use LIB\Session;
use phphtml5\Basic\Div;
use phphtml5\Basic\Span;
use phphtml5\Enums\Charset;
use phphtml5\Enums\Language;
use phphtml5\Enums\MediaType;
use phphtml5\Enums\Rel;
use phphtml5\Html5Page;
use phphtml5\jQueryUI\Menu;
use phphtml5\Tags\LinkTag;
use phphtml5\Tags\ScriptTag;
use phphtml5\Tags\Tag;
use function exceptionHandler;
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 4-jul-2015
 * Time: 17:56:23
 */
class DefaultView extends Html5Page {
    private $title, $description;
    private $header, $navbar, $content, $footer;
    private $jquis;
            
    function __construct($title = NULL, $description = NULL) {
        try {
            if (is_null($title)) {
                $this->title = 'PHP-Mvc';
            } else {
                $this->title = "$title - PHP-Mvc";
            }
            if (is_null($description)) {
                $this->description = 'Korte beschrijving van PHP-Mvc';
            } else {
               $this->description = $description;
            }
            parent::__construct(new Language(Language::DUTCH),
                    new Charset(Charset::UTF8),$this->title,$this->description,
                    Config::getInstance()->getLocalWebsiteFile('favicon.ico'));
            $this->jquis = new jQueryUISwitcher();
            $this->getHead()->appendElement($theme = $this->jquis->getLinkTag());
            $this->addGlobalCss($this->head);
            $this->addGlobalJs($this->head);
            
            $this->createContainer($this->body);
//            $this->getBody()->appendElement($this->jquis->getForm($this->body));
//            $this->header = $this->getBody()->appendElement($header = new Div('header'));
//            
//            $content_wrapper = $this->getBody()->appendElement(new Div('content_wrapper'));
//            $this->navbar = $content_wrapper->appendElement(new Div('navbar'));
//            $this->addNavBar($this->navbar);
//            $this->content = $content_wrapper->appendElement(new Div('content'));
//            $this->footer = $this->getBody()->appendElement(new Div('footer'));
//            $this->getFooter()->appendElement(new Span($this->auto_copyright('Bart Verstraete', 2006)));
        } catch (Exception $exc) {
            exceptionHandler($exc);
//            var_dump($exc);
        }
    }

    /**
     * 
     * @return Div
     */
    function getContent() {
        return $this->content;
    }

    /**
     * 
     * @return Div
     */
    function getFooter() {
        return $this->footer;
    }

    /**
     * 
     * @return Div
     */
    function getHeader() {
        return $this->header;
    }
    
    private function addGlobalCss(Tag &$parent) {
        foreach (Config::getInstance()->getGlobalCss() as $key => $value) {
            $parent->appendElement($script  = new LinkTag(NULL,
            Config::getInstance()->getRoot($value), NULL, NULL,
            new Rel(Rel::STYLESHEET), NULL, new MediaType(MediaType::TEXTCSS)));
            $script->setId($key);
        }
    }
    private function addGlobalJs(Tag &$parent) {
        foreach (Config::getInstance()->getGlobalJs() as $key => $value) {
            $parent->appendElement($script  = new ScriptTag(NULL, NULL, NULL,
            Config::getInstance()->getRoot($value),
            new MediaType(MediaType::TEXTJAVASCRIPT)));
            $script->setId($key);
        }
    }
    private function addNavBar(Tag &$parent) {
        $arr['Home'] = Config::getInstance()->getRoot();
        $arr['About'] = Config::getInstance()->getRoot('About/');
        if (Session::get('loggedin')) {
            $arr['Dashboard']['Dashboard'] = Config::getInstance()->getRoot('Dashboard/');
            $arr['Dashboard']['Logout'] = Config::getInstance()->getRoot('Login/logout');
        } else {
            $arr['Login'] = Config::getInstance()->getRoot('Login/');
        }

        $parent->appendElement($menu = new Menu($parent,'nav-bar', $arr));
    }
    private function createContainer(Tag &$body) {
        $this->getBody()->appendElement($container = new Div('container'));
        
        $this->header = $container->appendElement(new Div('header'));
        $this->getHeader()->appendElement($this->jquis->getForm($body));
        
        $this->navbar = $container->appendElement(new Div('navbar'));
        $this->addNavBar($this->navbar);
        $this->content = $container->appendElement(new Div('content'));
        
        $this->footer = $container->appendElement(new Div('footer'));
        $this->getFooter()->appendElement(new Span($this->auto_copyright('Bart Verstraete', 2006)));
    }
}