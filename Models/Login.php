<?php
namespace Models;

use LIB\Config;
use LIB\Model;
use LIB\Session;
use PDO;
use PDOException;
/**
 * Description of Login_Model
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class Login  extends Model {
    public function __construct() {
        parent::__construct();
//        echo 'Login_Model';
    }
    
    public function login() {
        echo 'from login()';
        var_dump($_POST);
        try {
            if (!key_exists('Login', $_POST)) {
                return FALSE;
            }
            $conn = $this->getDb();
            $stmt = $conn->prepare('select case when count(*) > 0
                                                then true
                                                else false
                                           end as approved
                                    from user as U
                                    where U.username = :username and U.password = md5(:password)');
            $stmt->bindValue(':username', $_POST['username']);
            $stmt->bindValue(':password', $_POST['password']);
            $stmt->execute();
            if ($stmt->fetchAll(PDO::FETCH_ASSOC)[0]['approved']) {
                Session::set('loggedin', TRUE);
                header(sprintf('Location: %s', Config::getInstance()->getRoot('Dashboard/')));
                return TRUE;
            } else {
                header(sprintf('Location: %s', Config::getInstance()->getRoot('Login/')));
                return FALSE;
            }
        } catch (PDOException $exc) {
            return FALSE;
        }
            
    }
}
