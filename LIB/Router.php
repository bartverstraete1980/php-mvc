<?php
namespace LIB;
/**
 * Description of Router
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class Router {
    private $url = [];
    private $args = [];


    public function __construct() {
        $this->url = explode('/', rtrim(isset($_GET['url']) ? $_GET['url'] : null, '/'));
        if (count($this->url) > 2) {
            $this->args = array_slice($this->url, 2);
//            TODO
//            var_dump($this->args);
//            die;
        }
    }
    
    public function getController() {
        if (!empty($this->url) &&  !empty($this->url[0])) {
            return $this->url[0];
        } else {
            return 'Home';
        }
    }
    public function getMethod() {
        if (count($this->url) > 1 && !empty($this->url[1])) {
            return $this->url[1];
        } else {
            return 'index';
        }
    }
    /**
     * 
     * @return Array
     */
    public function getArgs() {
        return $this->args;
    }
}
