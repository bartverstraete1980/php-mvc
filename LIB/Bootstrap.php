<?php
namespace LIB;

use LIB\Config;
use LIB\Router;
//include_once 'Config.php';
/**
 * Description of PHP-Mvc
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class Bootstrap {
    private $config;
    private $router;
    private $controller;
    
    public function __construct() {
        $this->config = Config::getInstance();
        $this->router = new Router();
        $this->run();
    }
    private function run() {
        $args = [];
        switch ($this->router->getController()) {
        case 'js':
            $this->controller = new \Controllers\Home();
            $args[] = $this->router->getMethod();
            break;
        case 'css':
            $this->controller = new \Controllers\Home();
            $args[] = $this->router->getMethod();
            break;
        default:
            $class = sprintf('Controllers\\%s', $this->router->getController());
            $this->controller = new $class();
            $this->controller->loadModel($this->router->getController());
            $args = $this->router->getArgs();
        }
        $this->checkArgs($args, in_array($this->router->getController(), ['css',
                                         'js']) ?
                                $this->router->getController() :
                                $this->router->getMethod());
    }
    public function checkArgs($args, $method) {
        if (method_exists($this->controller, $method)) {
            if (count($args) > 0) {
                $this->controller->$method(...$args);
            } else {
                $this->controller->$method();
            }
        }
    }
    public static function autoLoader($class) {
        $filename = str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
        include($filename);
    }
}
spl_autoload_register(__NAMESPACE__ . '\Bootstrap::autoLoader');
new Bootstrap();