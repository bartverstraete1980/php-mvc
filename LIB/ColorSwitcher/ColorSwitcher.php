<?php

namespace ColorSwitcher;

/**
 * Description of ColorSwitcher
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class ColorSwitcher {
    private static $instance;
    public $currrentcss;

    protected function __construct() {
        $this->currrentcss = Config::getInstance()->getDefaultCss();
        if (\array_key_exists('ColorSwitcher_css_file', $_POST)) {
            $this->currrentcss = $_POST['ColorSwitcher_css_file'];
        }
    }
    /**
     * 
     * @return ColorSwitcher
     */
    public static function getInstance(){
        if (static::$instance === NULL) {
            static::$instance = new static();
        }
        return static::$instance;
    }
    /**
     * 
     * @return \phphtml5\Tags\LinkTag
     */
    public function getLinkTag() {
        if ($this->currrentcss != '.') {
            return new \phphtml5\Tags\LinkTag(null,
                    \Configuration\Config::getInstance()->getLocalWebsiteFile("css/ColorSwitcher/$this->currrentcss"),
                    null,
                    null,
                    new \phphtml5\Enums\Rel(\phphtml5\Enums\Rel::STYLESHEET),
                    null,
                    new \phphtml5\Enums\MediaType(\phphtml5\Enums\MediaType::TEXTCSS));
        } else {
            return null;
        }
    }
    /**
     * 
     * @return \phphtml5\Basic\Div
     */
    public function getColorSwitcherDiv(\phphtml5\Tags\Tag &$parent = NULL) {
        $div = new \phphtml5\Basic\Div('ColorSwitcher', $parent);
        $div->appendElement($form = new \phphtml5\Basic\Form('ColorSwitcher',
                NULL,
                new \phphtml5\Enums\Method(\phphtml5\Enums\Method::POST)));
        $form->appendElement($labelselect = new \phphtml5\Basic\LabelSelect(Config::getInstance()->getStyles(),
                "{$form->getId()}",
                'ColorSwitcher', 
                'ColorSwitcher_css_file',
                $form,
                $this->currrentcss));
        $form->appendElement($submitbutton = new \phphtml5\Basic\SubmitButton('OK'));
        return $div;
    }
}
