<?php
namespace ColorSwitcher;

/**
 * Reads config.ini
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class Config {
    private static $instance;
    private $ini_array = [];
    
    protected function __construct() {
        $this->ini_array = parse_ini_file('ColorSwitcher/config.ini', TRUE);
    }
    /**
     * 
     * @return Config
     */
    public static function getInstance() {
        if (static::$instance === NULL) {
            static::$instance = new static();
        }
        return static::$instance;
    }
    /**
     * 
     * @return string
     */
    public function getDefaultCss() {
        return $this->ini_array['Default']['css'];
    }
    /**
     * 
     * @return array
     */
    public function getStyles() {
        $tmp['.'] = 'NONE';
        if (($handle = \opendir('css/ColorSwitcher'))) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != '.' && $entry != '..') {
                    $tmp[$entry] = preg_replace('/\.css$/i', '', $entry);
                }
            }
            \closedir($handle);
        }
        return $tmp;
    }
}
