<?php
namespace jQueryUISwitcher;

use jQueryUISwitcher\Config;
use LIB\Session;
use phphtml5\Enums\MediaType;
use phphtml5\Enums\Method;
use phphtml5\Enums\Rel;
use phphtml5\jQueryUI\Select;
use phphtml5\Tags\FormTag;
use phphtml5\Tags\LinkTag;
use phphtml5\Tags\Tag;
/**
 * Description of jQueryUISwitcher
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class jQueryUISwitcher {
    private $config;
    private  $theme;
    public function __construct() {
        $this->config = new Config();
        if (isset($_POST['jQUIS_Select'])) {
            Session::set('theme', $_POST['jQUIS_Select']);
        }
        $this->theme = Session::get('theme') ? Session::get('theme') :
            $this->config->getDefaultTheme();
    }
    public function getLinkTag() {
        return new LinkTag(NULL, $this->config->getTheme($this->theme), NULL, NULL,
                new Rel(Rel::STYLESHEET), NULL, new MediaType(MediaType::TEXTCSS));
    }
    private function getSelect(Tag &$parent) {
        $s = new Select($parent, 'jQUIS',
                $this->config->getThemesKeys(), $this->theme, 'jQUIS_Form');
        return $s;
    }
    public function getForm(Tag &$parent) {
        $f = new FormTag(NULL, NULL, NULL, NULL, new Method(Method::POST), 'jQUIS', NULL, NULL, $parent);
        $f->setId('jQUIS_Form');
        $f->appendElement($ls = $this->getSelect($f));
        return $f;
    }
}