<?php
namespace jQueryUISwitcher;

/**
 * Description of Config
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class Config {
    private $ini_array = [];
    
    public function __construct() {
        $this->ini_array = parse_ini_file('jQueryUISwitcher/config.ini', TRUE);
//        var_dump($this->ini_array);
//        foreach ($this->ini_array['themes']['theme'] as $key => $value) {
//            echo "$key, $value\n";
//        }
    }
    /**
     * 
     * @return string
     */
    public function getDefaultTheme() {
        return (string)$this->ini_array['themes']['default'];
    }
    /**
     * 
     * @param string $name
     * @return string
     */
    public function getTheme($name) {
        return (string)$this->ini_array['themes']['theme'][$name];
    }
    /**
     * 
     * @return array
     */
    public function getThemes() {
        return (array)$this->ini_array['themes']['theme'];
    }
    public function getThemesKeys() {
        $tmp = [];
        foreach ($this->ini_array['themes']['theme'] as $key => $value) {
            $tmp[$key] = $key;
        }
        return $tmp;
    }
}
