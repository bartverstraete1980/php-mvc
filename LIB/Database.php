<?php
namespace LIB;

use PDO;
use LIB\Config;
/**
 * Description of Database
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */
class Database extends PDO {
    private $dbh;

    public function __construct() {
        $server = Config::getInstance()->getMySQLServer();
        $port = Config::getInstance()->getMySQLPort();
        $username = Config::getInstance()->getMySQLUsername();
        $password = Config::getInstance()->getMySQLPassword();
        $database = Config::getInstance()->getMySQLDatabase();;
        parent::__construct("mysql:host=$server;port=$port;dbname=$database", $username, $password);
    }
}