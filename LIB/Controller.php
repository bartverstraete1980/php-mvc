<?php
namespace LIB;

/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be * Date: 28-jun-2015
 * Time: 16:16:45
 */
class Controller {

    private $model, $view;
    private $cssPath = 'Views/css';
    private $jsPath = 'Views/js';
    
    function __construct() {
        Session::init();
        $this->view = new View();
    }

    public function loadModel($name) {
        $path = "Models/{$name}.php";
        if (file_exists($path)) {
            $model_class = "Models\\{$name}";
            $this->model = new $model_class();
        }
    }

    /**
     * 
     * @return Model
     */
    function getModel() {
        return $this->model;
    }

    /**
     * 
     * @return View
     */
    function getView() {
        return $this->view;
    }

    public function css($file) {
        $fp = "$this->cssPath/$file";
        if (file_exists($fp)) {
            header('Content-type: text/css');
            readfile($fp);
        }
    }
    public function js($file) {
        $fp = "$this->jsPath/$file";
        if (file_exists($fp)) {
            header('Content-type: text/javascript');
            readfile($fp);
        }
    }
}
