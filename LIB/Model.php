<?php
namespace LIB;
/**
 * Created by Netbeans
 * @author Bart Verstraete<bart.verstraete@student.howest.be>
 * Date: 29-jun-2015
 * Time: 7:56:41
 */
class Model {
    private $db;

    function __construct() {
        $this->db = new Database();
    }

    /**
     * 
     * @return Database
     */
     function getDb() {
        return $this->db;
    }

}
