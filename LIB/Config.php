<?php
namespace LIB;
/**
 * Description of config
 *
 * @author Bart Verstraete <bart.verstraete@student.howest.be>
 */

class Config {
    private static $instance;
    //holds the values of config.ini
    private $ini_array = [];
    
    protected function __construct() {
        $this->ini_array= parse_ini_file("config.ini", TRUE);
        if (is_array($this->ini_array)) {
            $this->addIncludes(); 
//            $this->addModules();
        }
    }
    /**
     * 
     * @return void
     */
    private function __clone() {
        
    }
    /**
     * 
     * @return void
     */
    private function __wakeup() {
        
    }
    /**
     * 
     */
    private function addIncludes() {
        foreach ($this->ini_array['PHP-Mvc_Settings']['include_dirs'] as  $value) {
            set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__DIR__) . $value);
        }
    }
    private function addModules() {
        foreach ($this->ini_array['Modules']['load_modules'] as $value) {
            $class ="$value\\$value";
            $class::getInstance();
        }
    }

//    public static function autoLoader($class) {
//        $filename = str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
//        include($filename);
//    }
    /**
     * 
     * @return Config
     */
    public static function getInstance() {
        if (static::$instance === NULL) {
            static::$instance = new static();
        }
        return static::$instance;
    }
    /**
     * 
     * @param string $path
     * @return string
     */
    public function getRoot($path = '') {
        return "{$this->ini_array['Server_Settings']['ROOT']}$path";
    }
    /**
     * 
     * @param string $filename
     * @return string
     */
    public function getLocalWebsiteFile($filename = '') {
        if (file_exists($filename)){
            return "{$this->getRoot()}$filename";
        } else {
            // TODO Throw an Exceotion
            return $this->getRoot();
        }
    }
    // MySQL
    public function getMySQLServer() {
        if (array_key_exists('MySQL', $this->ini_array)) {
            if (array_key_exists('server', $this->ini_array['MySQL'])) {
                return $this->ini_array['MySQL']['server'];
            }
        }
    }
    public function getMySQLPort() {
        if (array_key_exists('MySQL', $this->ini_array)) {
            if (array_key_exists('port', $this->ini_array['MySQL'])) {
                return $this->ini_array['MySQL']['port'];
            }
        }
    }
    public function getMySQLDatabase() {
        if (array_key_exists('MySQL', $this->ini_array)) {
            if (array_key_exists('database', $this->ini_array['MySQL'])) {
                return $this->ini_array['MySQL']['database'];
            }
        }
    }
    public function getMySQLUsername() {
        if (array_key_exists('MySQL', $this->ini_array)) {
            if (array_key_exists('username', $this->ini_array['MySQL'])) {
                return $this->ini_array['MySQL']['username'];
            }
        }
    }
    public function getMySQLPassword() {
        if (array_key_exists('MySQL', $this->ini_array)) {
            if (array_key_exists('password', $this->ini_array['MySQL'])) {
                return $this->ini_array['MySQL']['password'];
            }
        }
    }
    public function getGlobalCss() {
        return $this->ini_array['css'];
    }
    public function getGlobalJs() {
        return $this->ini_array['js'];
    }
}
//spl_autoload_register(__NAMESPACE__ . '\Config::autoLoader');
